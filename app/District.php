<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
   public function subcounty($value='')
   {
     return $this->hasMany('App\SubCounty');
   }

   public function region($value='')
   {
   	return $this->belongsTo('App\Region','region_id');
   }
}
