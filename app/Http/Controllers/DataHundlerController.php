<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;
use App\District;
use App\SubCounty;
use App\Parish;
use App\Record;
class DataHundlerController extends Controller
{

    public function data_handler(Request $request)
    {
        $date_from_phone = json_decode($request->DATA,true);
    
        foreach ($date_from_phone as $value_data) {

          try {            
            $phone_id = $value_data['phone_id'];
            $staffName = $value_data['staffName'];
            $date_recorded = $value_data['date_recorded'];
            $ageCategory = $value_data['ageCategory'];
            $gender = $value_data['gender'];
            $Agric_service_best = $value_data['Agric_service_best'];
            $agric_service_spec = $value_data['agric_service_spec'];
            $agric_service_reason = $value_data['agric_service_reason'];
            $agric_service_worst = $value_data['agric_service_worst'];
            $agric_service_worst_spec = $value_data['agric_service_worst_spec'];
            $agric_service_reason_worst = $value_data['agric_service_reason_worst'];
            $Agric_service_problem = $value_data['Agric_service_problem'];
            $Agric_service_problem_spec = $value_data['Agric_service_problem_spec'];
            $Agric_service_reason_problem = $value_data['Agric_service_reason_problem'];

            $Agric_service_need = $value_data['Agric_service_need'];
            $Agric_service_need_spec = $value_data['Agric_service_need_spec'];
            $Agric_service_reason_need = $value_data['Agric_service_reason_need'];

            $Agric_proposed_priority = $value_data['Agric_proposed_priority'];
            $Agric_proposed_priority_spec = $value_data['Agric_proposed_priority_spec'];
            $Agric_proposed_priority_reason = $value_data['Agric_proposed_priority_reason'];

            $id = $value_data['id'];
            
            $region_name = ucwords(strtolower($value_data['region']));
            $district_name = ucwords(strtolower($value_data['district']));
            $sub_county_name = ucwords(strtolower($value_data['subcounty']));
            $parish_name = ucwords(strtolower($value_data['parish']));

             } catch (\Exception $e) {
                   echo $e->getMessage();
            }



            if (Region::all()->where('name',$region_name)->count() == 0) {
                $save_region = new Region();
                $save_region->name = $region_name;
                $save_region->save();

                // save_the distrct
                $save_district = new District();
                $save_district->name = $district_name;
                $save_district->region_id = $save_region->id;
                $save_district->save(); 

                $save_sub_county = new SubCounty();
                $save_sub_county->name = $sub_county_name; 
                $save_sub_county->district_id = $save_district->id;
                $save_sub_county->save();

                // $save_parish = new Parish();
                // $save_parish->name = $parish_name;
                // $save_parish->sub_county_id = $save_sub_county->id;
                // $save_parish->save();

                $this->save_record($id,$phone_id,$staffName,$date_recorded,$ageCategory,$Agric_service_best,$agric_service_spec,$agric_service_reason,$agric_service_worst,$agric_service_worst_spec,$agric_service_reason_worst,$Agric_service_problem,$Agric_service_problem_spec,$Agric_service_reason_problem,$save_sub_county->id,$gender,$Agric_service_need,$Agric_service_need_spec,$Agric_service_reason_need,$Agric_proposed_priority,$Agric_proposed_priority_spec,$Agric_proposed_priority_reason);

            }
            else{
                // read the id of that region
                $region = Region::all()->where('name',$region_name)->last();
                
                if (District::all()->where('name',$district_name)->where('region_id',$region->id)->count() == 0 ) {

                    $save_district = new District();
                    $save_district->name = $district_name;
                    $save_district->region_id = $region->id;
                    $save_district->save();

                    $save_sub_county = new SubCounty();
                    $save_sub_county->name = $sub_county_name; 
                    $save_sub_county->district_id = $save_district->id;
                    $save_sub_county->save();

                    // $save_parish = new Parish();
                    // $save_parish->name = $parish_name;
                    // $save_parish->sub_county_id = $save_sub_county->id;
                    // $save_parish->save();

                    $this->save_record($id,$phone_id,$staffName,$date_recorded,$ageCategory,$Agric_service_best,$agric_service_spec,$agric_service_reason,$agric_service_worst,$agric_service_worst_spec,$agric_service_reason_worst,$Agric_service_problem,$Agric_service_problem_spec,$Agric_service_reason_problem,$save_sub_county->id,$gender,$Agric_service_need,$Agric_service_need_spec,$Agric_service_reason_need,$Agric_proposed_priority,$Agric_proposed_priority_spec,$Agric_proposed_priority_reason);

                }
                else{
                    $district = District::all()->where('name',$district_name)->where('region_id',$region->id)->last();         
                    if (SubCounty::all()->where('name',$sub_county_name)->where('district_id',$district->id)->count() == 0) {

                        $save_sub_county = new SubCounty();
                        $save_sub_county->name = $sub_county_name; 
                        $save_sub_county->district_id = $district->id;
                        $save_sub_county->save();

                        // $save_parish = new Parish();
                        // $save_parish->name = $parish_name;
                        // $save_parish->sub_county_id = $save_sub_county->id;
                        // $save_parish->save();

                        $this->save_record($id,$phone_id,$staffName,$date_recorded,$ageCategory,$Agric_service_best,$agric_service_spec,$agric_service_reason,$agric_service_worst,$agric_service_worst_spec,$agric_service_reason_worst,$Agric_service_problem,$Agric_service_problem_spec,$Agric_service_reason_problem,$save_sub_county->id,$gender,$Agric_service_need,$Agric_service_need_spec,$Agric_service_reason_need,$Agric_proposed_priority,$Agric_proposed_priority_spec,$Agric_proposed_priority_reason);
                        
                    }
                    else{

                        $sub_county = SubCounty::all()->where('name',$sub_county_name)->where('district_id',$district->id)->last();                       

                        if (Parish::all()->where('name',$parish_name)->where('sub_county_id',$sub_county->id)->count() == 0) {
                            $save_parish = new Parish();
                            $save_parish->name = $parish_name;
                            $save_parish->sub_county_id = $sub_county->id;
                            $save_parish->save();

                            $this->save_record($id,$phone_id,$staffName,$date_recorded,$ageCategory,$Agric_service_best,$agric_service_spec,$agric_service_reason,$agric_service_worst,$agric_service_worst_spec,$agric_service_reason_worst,$Agric_service_problem,$Agric_service_problem_spec,$Agric_service_reason_problem,$sub_county->id,$gender,$Agric_service_need,$Agric_service_need_spec,$Agric_service_reason_need,$Agric_proposed_priority,$Agric_proposed_priority_spec,$Agric_proposed_priority_reason);
                        }
                        else{
                            $parish = Parish::all()->where('name',$parish_name)->where('sub_county_id',$sub_county->id)->last();
                            $parish_id = $parish->id;

                           $this->save_record($id,$phone_id,$staffName,$date_recorded,$ageCategory,$Agric_service_best,$agric_service_spec,$agric_service_reason,$agric_service_worst,$agric_service_worst_spec,$agric_service_reason_worst,$Agric_service_problem,$Agric_service_problem_spec,$Agric_service_reason_problem,$sub_county,$gender,$Agric_service_need,$Agric_service_need_spec,$Agric_service_reason_need,$Agric_proposed_priority,$Agric_proposed_priority_spec,$Agric_proposed_priority_reason);
                        }

                    }
                }

            }
        }
        echo "Your new data has been saved on the remote database. Thank you";
    }

    public function save_record($id,$phone_id,$staffName,$date_recorded,$ageCategory,$Agric_service_best,$agric_service_spec,$agric_service_reason,$agric_service_worst,$agric_service_worst_spec,$agric_service_reason_worst,$Agric_service_problem,$Agric_service_problem_spec,$Agric_service_reason_problem,$parish_id,$gender,$Agric_service_need,$Agric_service_need_spec,$Agric_service_reason_need,$Agric_proposed_priority,$Agric_proposed_priority_spec,$Agric_proposed_priority_reason)
    {
        $save_record = new Record();
        $save_record->sub_county_id = $parish_id;
        $save_record->record_id = $id."".$phone_id;
        $save_record->staffName = $staffName;

        $to_date = date_create(str_replace("/", "-", $date_recorded));
        $attempt_date = date_timestamp_get($to_date);

        $save_record->date_recorded = $attempt_date;
        $save_record->ageCategory = $ageCategory;
        $save_record->gender = $gender;
        $save_record->Agric_service_best = $Agric_service_best;
        $save_record->agric_service_spec = $agric_service_spec;
        $save_record->agric_service_reason = $agric_service_reason;
        $save_record->agric_service_worst = $agric_service_worst;
        $save_record->agric_service_worst_spec = $agric_service_worst_spec;
        $save_record->agric_service_reason_worst = $agric_service_reason_worst;
        $save_record->Agric_service_problem = $Agric_service_problem;
        $save_record->Agric_service_problem_spec = $Agric_service_problem_spec;
        $save_record->Agric_service_reason_problem = $Agric_service_reason_problem;
        $save_record->Agric_service_need = $Agric_service_need;
        $save_record->Agric_service_need_spec = $Agric_service_need_spec;
        $save_record->Agric_service_reason_need = $Agric_service_reason_need;
        $save_record->Agric_proposed_priority = $Agric_proposed_priority;
        $save_record->Agric_proposed_priority_spec = $Agric_proposed_priority_spec;
        $save_record->Agric_proposed_priority_reason = $Agric_proposed_priority_reason;
        try {
            $save_record->save();            
        } catch (\Exception $e) {} 
    }





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Record::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
