<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Education;
use App\Region;
use App\District;
use App\SubCounty;

class EducationController extends Controller
{
    public function education_data_handler(Request $request)
    {
        $date_from_phone = json_decode($request->DATA,true);
        foreach ($date_from_phone as $value_data) {     
            $id = $value_data['id'];
            $region_name = ucwords(strtolower($value_data['region']));
            $district_name = ucwords(strtolower($value_data['district']));
            $sub_county_name = ucwords(strtolower($value_data['subcounty']));           
            $ageCategory = $value_data['ageCategory'];
            $gender = $value_data['gender'];         
            $phone_id = $value_data['phone_id'];         
            $level_of_education = $value_data['level_of_education'];
            $use_education = $value_data['use_education'];
            $best_use_education_service = $value_data['best_use_education_service'];
            $best_use_education_spec = $value_data['best_use_education_spec'];
            $best_use_education_reason = $value_data['best_use_education_reason'];
            $worst_use_education_service = $value_data['worst_use_education_service'];
            $worst_use_education_spec = $value_data['worst_use_education_spec'];
            $worst_use_education_reason = $value_data['worst_use_education_reason'];
            $priority_use_education_service = $value_data['priority_use_education_service'];
            $priority_use_education_spec = $value_data['priority_use_education_spec'];
            $priority_use_education_reason = $value_data['priority_use_education_reason'];

            if (Region::all()->where('name',$region_name)->count() == 0) {
                $save_region = new Region();
                $save_region->name = $region_name;
                $save_region->save();

                // save_the distrct
                $save_district = new District();
                $save_district->name = $district_name;
                $save_district->region_id = $save_region->id;
                $save_district->save(); 

                $save_sub_county = new SubCounty();
                $save_sub_county->name = $sub_county_name; 
                $save_sub_county->district_id = $save_district->id;
                $save_sub_county->save();

                $this->save_data($save_sub_county->id,$id,$phone_id,$ageCategory,$gender,$level_of_education,$use_education,$best_use_education_service,$best_use_education_spec,$best_use_education_reason,$worst_use_education_service,$worst_use_education_spec,$worst_use_education_reason,$priority_use_education_service,$priority_use_education_spec,$priority_use_education_reason);

            }
            else{
                $region = Region::all()->where('name',$region_name)->last();
                
                if (District::all()->where('name',$district_name)->where('region_id',$region->id)->count() == 0 ) {

                    $save_district = new District();
                    $save_district->name = $district_name;
                    $save_district->region_id = $region->id;
                    $save_district->save();

                    $save_sub_county = new SubCounty();
                    $save_sub_county->name = $sub_county_name; 
                    $save_sub_county->district_id = $save_district->id;
                    $save_sub_county->save();                 

                  $this->save_data($save_sub_county->id,$id,$phone_id,$ageCategory,$gender,$level_of_education,$use_education,$best_use_education_service,$best_use_education_spec,$best_use_education_reason,$worst_use_education_service,$worst_use_education_spec,$worst_use_education_reason,$priority_use_education_service,$priority_use_education_spec,$priority_use_education_reason);

                }else{
                    $district = District::all()->where('name',$district_name)->where('region_id',$region->id)->last();         
                    if (SubCounty::all()->where('name',$sub_county_name)->where('district_id',$district->id)->count() == 0) {

                        $save_sub_county = new SubCounty();
                        $save_sub_county->name = $sub_county_name; 
                        $save_sub_county->district_id = $district->id;
                        $save_sub_county->save(); 

                        $this->save_data($save_sub_county->id,$id,$phone_id,$ageCategory,$gender,$level_of_education,$use_education,$best_use_education_service,$best_use_education_spec,$best_use_education_reason,$worst_use_education_service,$worst_use_education_spec,$worst_use_education_reason,$priority_use_education_service,$priority_use_education_spec,$priority_use_education_reason);                       
                    }else{
                        $save_sub_county = SubCounty::all()->where('name',$sub_county_name)->where('district_id',$district->id)->last();

                         $this->save_data($save_sub_county->id,$id,$phone_id,$ageCategory,$gender,$level_of_education,$use_education,$best_use_education_service,$best_use_education_spec,$best_use_education_reason,$worst_use_education_service,$worst_use_education_spec,$worst_use_education_reason,$priority_use_education_service,$priority_use_education_spec,$priority_use_education_reason);

                    }

                }
            } 
        } 

        echo "Your data has been successfully uploaded to the remote server.";          
    }


    public function save_data($sub_county_id,$id,$phone_id,$ageCategory,$gender,$level_of_education,$use_education,$best_use_education_service,$best_use_education_spec,$best_use_education_reason,$worst_use_education_service,$worst_use_education_spec,$worst_use_education_reason,$priority_use_education_service,$priority_use_education_spec,$priority_use_education_reason)
    {

     
         $save_healthandeducation = new Education();
         $save_healthandeducation->record_id = $id."".$phone_id;
         $save_healthandeducation->ageCategory = $ageCategory;
         $save_healthandeducation->gender = $gender;
         $save_healthandeducation->level_of_education = $level_of_education;         
         $save_healthandeducation->use_education = $use_education;
         $save_healthandeducation->best_use_education_service = $best_use_education_service;
         $save_healthandeducation->best_use_education_spec = $best_use_education_spec;
         $save_healthandeducation->best_use_education_reason = $best_use_education_reason;
         $save_healthandeducation->worst_use_education_service = $worst_use_education_service;
         $save_healthandeducation->worst_use_education_spec = $worst_use_education_spec;
         $save_healthandeducation->worst_use_education_reason = $worst_use_education_reason;
         $save_healthandeducation->priority_use_education_service = $priority_use_education_service;
         $save_healthandeducation->priority_use_education_spec = $priority_use_education_spec;
         $save_healthandeducation->priority_use_education_reason = $priority_use_education_reason;
         $save_healthandeducation->sub_county_id = $sub_county_id;
         try {
            $save_healthandeducation->save(); 
         } catch (\Exception $e) {
             
         }
         

    }
    public function index()
    {
        return view("pages.education")->with(['health_and_education'=>Education::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
