<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCounty;
use App\Record;

class SubCountyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subcounty = SubCounty::find($id);


       // read the subcountyes in this region, and count users per qn
       $read_recordAgric_service_best_male =  $read_recordAgric_service_best_female = $read_recordAgric_service_best_ageCategory_b18 = $read_recordAgric_service_best_ageCategory_b29 = $read_recordAgric_service_best_ageCategory_b30 = $read_recordAgric_service_best_ageCategory_a30 = 0;

       $read_recordAgric_service_best_Seeds_male =  $read_recordAgric_service_best_Seeds_female = $read_recordAgric_service_best_Seeds_ageCategory_b18 = $read_recordAgric_service_best_Seeds_ageCategory_b29 = $read_recordAgric_service_best_Seeds_ageCategory_b30 = $read_recordAgric_service_best_Seeds_ageCategory_a30 = 0;

       $read_recordAgric_service_best_Fertilizers_male =  $read_recordAgric_service_best_Fertilizers_female = $read_recordAgric_service_best_Fertilizers_ageCategory_b18 = $read_recordAgric_service_best_Fertilizers_ageCategory_b29 = $read_recordAgric_service_best_Fertilizers_ageCategory_b30 = $read_recordAgric_service_best_Fertilizers_ageCategory_a30 = 0;

       $read_recordAgric_service_best_Pesticides_male =  $read_recordAgric_service_best_Pesticides_female = $read_recordAgric_service_best_Pesticides_ageCategory_b18 = $read_recordAgric_service_best_Pesticides_ageCategory_b29 = $read_recordAgric_service_best_Pesticides_ageCategory_b30 = $read_recordAgric_service_best_Pesticides_ageCategory_a30 = 0;


        $read_recordAgric_service_best_Water_for_production_male =  $read_recordAgric_service_best_Water_for_production_female =  0;

       $read_recordAgric_service_best_Advisory_services_male =  $read_recordAgric_service_best_Advisory_services_female =  0;

        $read_recordAgric_service_best_Access_to_Credit_and_capital_male =  $read_recordAgric_service_best_Access_to_Credit_and_capital_female = 0;

        $read_recordAgric_service_best_Agricultural_mechanization_male =  $read_recordAgric_service_best_Agricultural_mechanization_female =  0;

        $read_recordAgric_service_best_Market_linkages_male =  $read_recordAgric_service_best_Market_linkages_female = 0;

        $read_recordAgric_service_best_Capacity_building_male =  $read_recordAgric_service_best_Capacity_building_female = 0;

        $read_recordAgric_service_best_value_addition_male =  $read_recordAgric_service_best_value_addition_female =  0;

        $read_recordAgric_service_best_address_challenges_male =  $read_recordAgric_service_best_address_challenges_female = 0;

       $gender_arrayAgric_service_best_age_Agric_service_best =  $gender_arrayAgric_service_best_age_seed_best = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_seed =  $agric_service_best_age_seed_best_seed = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_Pesticides =  $agric_service_best_age_seed_best_Pesticides = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_Fertilizers =  $agric_service_best_age_seed_best_Fertilizers = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_Advisory_services =  $agric_service_best_age_seed_best_Advisory_services = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_Market_linkages =  $agric_service_best_age_seed_best_Market_linkages = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_Capacity_building =  $agric_service_best_age_seed_best_Capacity_building = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_value_addition =  $agric_service_best_age_seed_best_value_addition = array();
       $offerArray = array();

       $gender_arrayAgric_service_best_age_Agric_service_best_Water_for_production =  $agric_service_best_age_seed_best_Water_for_production = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_Access_to_Credit_and_capital =  $agric_service_best_age_seed_best_Access_to_Credit_and_capital = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_Agricultural_mechanization =  $agric_service_best_age_seed_best_Agricultural_mechanization = array();
       $gender_arrayAgric_service_best_age_Agric_service_best_address_challenges =  $agric_service_best_age_seed_best_address_challenges = array();

       // ==================================================================================================
       $read_recordAgric_service_best_male_worst = $read_recordAgric_service_best_female_worst = $read_recordAgric_service_worst_Seeds_male = $read_recordAgric_service_worst_Seeds_female = $read_recordAgric_service_worst_Pesticides_male = $read_recordAgric_service_worst_Pesticides_female =  $read_recordAgric_service_worst_Fertilizers_male = $read_recordAgric_service_worst_Fertilizers_female = $read_recordAgric_service_worst_Advisory_services_male = $read_recordAgric_service_worst_Advisory_services_female = $read_recordAgric_service_worst_Water_for_production_male = $read_recordAgric_service_worst_Water_for_production_female = $read_recordAgric_service_worst_Access_to_Credit_and_capital_male = $read_recordAgric_service_worst_Access_to_Credit_and_capital_female = $read_recordAgric_service_worst_Agricultural_mechanization_male = $read_recordAgric_service_worst_Agricultural_mechanization_female = $read_recordAgric_service_worst_Market_linkages_male = $read_recordAgric_service_worst_Market_linkages_female = $read_recordAgric_service_worst_Capacity_building_male =  $read_recordAgric_service_worst_Capacity_building_female =  $read_recordAgric_service_worst_value_addition_female = $read_recordAgric_service_worst_value_addition_male =  $read_recordAgric_service_worst_address_challenges_male = $read_recordAgric_service_worst_address_challenges_female = $read_male= $read_female =$read_ageCategory_b18 = $read_ageCategory_b29 = $read_ageCategory_b30 = $read_ageCategory_a30 = 0;

       $worst_service = $gender = $age=array();

       $problem = $priority = array();
       $Agric_service_problem_Agricultural_extension_services = $Agric_service_problem_Seeds = $Agric_service_problem_Pesticides = $Agric_service_problem_Fertilizers = $Agric_service_problem_Advisory_services = $Agric_service_problem_Water_for_production = $Agric_service_problem_Access_to_Credit_and_capital = $Agric_service_problem_Agricultural_mechanization = $Agric_service_problem_Market_linkages = $Agric_service_problem_capacity_building = $Agric_service_problem_Market_value_addition = $Agric_service_problem_Market_address_challange = 0;


         $Agric_proposed_priority_Seeds = $Agric_Pesticides = $Agric_Fertilizers = $Agric_Advisory_services = $Agric_Water_for_production = $Agric_Access_Credit_capital = $Agric_Agricultural_mechanization = $Agric_Market_linkages = $Agric_Capacity_building = $Agric_value_addition = $Agric_Support_address = 0;

       
             
                // Agricultural Extension services QN.1 best

                $read_male = $read_male + Record::all()->where('sub_county_id',$subcounty->id)->where('gender','Male')->count();

                $read_female = $read_female + Record::all()->where('sub_county_id',$subcounty->id)->where('gender','Female')->count();

                $read_ageCategory_b18 = $read_ageCategory_b18 + Record::all()->where('sub_county_id',$subcounty->id)->where('ageCategory','Below 18 years')->count();

                $read_ageCategory_b29 = $read_ageCategory_b29 + Record::all()->where('sub_county_id',$subcounty->id)->where('ageCategory','18 to 29 years')->count();

                $read_ageCategory_b30 = $read_ageCategory_b30 + Record::all()->where('sub_county_id',$subcounty->id)->where('ageCategory','30 to 35 years')->count();

                $read_ageCategory_a30 =  $read_ageCategory_a30 + Record::all()->where('sub_county_id',$subcounty->id)->where('ageCategory','Above 35 years')->count();

                $Agric_service_problem_Agricultural_extension_services = $Agric_service_problem_Agricultural_extension_services + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Agricultural Extension services')->count();

                $problem["Agricultural Extension services"] = $Agric_service_problem_Agricultural_extension_services;

                $Agric_service_problem_Seeds = $Agric_service_problem_Seeds + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Seeds')->count();

 
                $Agric_service_problem_Pesticides = $Agric_service_problem_Pesticides + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Pesticides')->count();

 
                $Agric_service_problem_Fertilizers = $Agric_service_problem_Fertilizers + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Fertilizers')->count();

 
                $Agric_service_problem_Advisory_services = $Agric_service_problem_Advisory_services + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Advisory services')->count();

 
                $Agric_service_problem_Water_for_production = $Agric_service_problem_Water_for_production + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Water for production')->count();

 
                $Agric_service_problem_Access_to_Credit_and_capital = $Agric_service_problem_Access_to_Credit_and_capital + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Access to Credit and capital')->count();

 
                $Agric_service_problem_Agricultural_mechanization = $Agric_service_problem_Agricultural_mechanization + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Agricultural mechanization')->count();

 
                $Agric_service_problem_Market_linkages = $Agric_service_problem_Market_linkages + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Market linkages')->count();

 
                $Agric_service_problem_capacity_building = $Agric_service_problem_capacity_building + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Capacity building opportunities in various agriculture value chains')->count();

 
                $Agric_service_problem_Market_value_addition = $Agric_service_problem_Market_value_addition + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','value addition tools at the sub county')->count();

 
                $Agric_service_problem_Market_address_challange = $Agric_service_problem_Market_address_challange + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_problem','Support to address challenges of climate change')->count();


                 // proposed
                $Agric_proposed_priority_Seeds = $Agric_proposed_priority_Seeds + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Seeds')->count();                

                $Agric_Pesticides = $Agric_Pesticides + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Pesticides')->count();

                $Agric_Fertilizers = $Agric_Fertilizers + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Fertilizers')->count();

                $Agric_Advisory_services = $Agric_Advisory_services + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Advisory services')->count();
                
                $Agric_Water_for_production = $Agric_Water_for_production + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Water for production')->count();                

                $Agric_Access_Credit_capital = $Agric_Access_Credit_capital + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Access to Credit and capital')->count();

                

                $Agric_Agricultural_mechanization = $Agric_Agricultural_mechanization + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Agricultural mechanization')->count();



                $Agric_Market_linkages = $Agric_Market_linkages + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Market linkages')->count();               

                $Agric_Capacity_building = $Agric_Capacity_building + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Capacity building opportunities in various agriculture value chains')->count();               
                $Agric_value_addition = $Agric_value_addition + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','value addition tools at the sub county')->count();               

                $Agric_Support_address = $Agric_Support_address + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_proposed_priority','Support to address challenges of climate change')->count();               

                // end profile

 
               // Seeds QN.1 best
                $read_recordAgric_service_best_Seeds_male = $read_recordAgric_service_best_Seeds_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Seeds')->where('gender','Male')->count();

                $read_recordAgric_service_best_male = $read_recordAgric_service_best_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Agricultural Extension services')->where('gender','Male')->count();

                $read_recordAgric_service_best_female = $read_recordAgric_service_best_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Agricultural Extension services')->where('gender','Female')->count();

                // worst
                $read_recordAgric_service_best_male_worst = $read_recordAgric_service_best_male_worst + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Agricultural Extension services')->where('gender','Male')->count();

                $read_recordAgric_service_best_female_worst = $read_recordAgric_service_best_female_worst + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Agricultural Extension services')->where('gender','Female')->count();            
               
               

                 // Seeds QN.1 best
                $read_recordAgric_service_best_Seeds_male = $read_recordAgric_service_best_Seeds_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Seeds')->where('gender','Male')->count();

                $read_recordAgric_service_best_Seeds_female = $read_recordAgric_service_best_Seeds_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Seeds')->where('gender','Female')->count();

                // Worst
                 $read_recordAgric_service_worst_Seeds_male = $read_recordAgric_service_worst_Seeds_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Seeds')->where('gender','Male')->count();

                $read_recordAgric_service_worst_Seeds_female = $read_recordAgric_service_worst_Seeds_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Seeds')->where('gender','Female')->count();
                // ==================================

                

                    // Pesticides QN.1 best
                $read_recordAgric_service_best_Pesticides_male = $read_recordAgric_service_best_Pesticides_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Pesticides')->where('gender','Male')->count();

                $read_recordAgric_service_best_Pesticides_female = $read_recordAgric_service_best_Pesticides_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Pesticides')->where('gender','Female')->count();

                // Worst
                $read_recordAgric_service_worst_Pesticides_male = $read_recordAgric_service_worst_Pesticides_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Pesticides')->where('gender','Male')->count();

                $read_recordAgric_service_worst_Pesticides_female = $read_recordAgric_service_worst_Pesticides_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Pesticides')->where('gender','Female')->count();
                // ======================================================================

               

                // Fertilizers QN.1 best
                $read_recordAgric_service_best_Fertilizers_male = $read_recordAgric_service_best_Fertilizers_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Fertilizers')->where('gender','Male')->count();

                $read_recordAgric_service_best_Fertilizers_female = $read_recordAgric_service_best_Fertilizers_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Fertilizers')->where('gender','Female')->count();

                // Worst
                $read_recordAgric_service_worst_Fertilizers_male = $read_recordAgric_service_worst_Fertilizers_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Fertilizers')->where('gender','Male')->count();

                $read_recordAgric_service_worst_Fertilizers_female = $read_recordAgric_service_worst_Fertilizers_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Fertilizers')->where('gender','Female')->count();
                // ==============================================================

               


                // Advisory services QN.1 best
                $read_recordAgric_service_best_Advisory_services_male = $read_recordAgric_service_best_Advisory_services_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Advisory services')->where('gender','Male')->count();

                $read_recordAgric_service_best_Advisory_services_female = $read_recordAgric_service_best_Advisory_services_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Advisory services')->where('gender','Female')->count();
                // Worst
                $read_recordAgric_service_worst_Advisory_services_male = $read_recordAgric_service_worst_Advisory_services_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Advisory services')->where('gender','Male')->count();

                $read_recordAgric_service_worst_Advisory_services_female = $read_recordAgric_service_worst_Advisory_services_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Advisory services')->where('gender','Female')->count();

                // ==================================================================================

                 

                 // Water for production QN.1 Best
                $read_recordAgric_service_best_Water_for_production_male = $read_recordAgric_service_best_Water_for_production_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Water for production')->where('gender','Male')->count();

                $read_recordAgric_service_best_Water_for_production_female = $read_recordAgric_service_best_Water_for_production_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Water for production')->where('gender','Female')->count();

                // worst

                $read_recordAgric_service_worst_Water_for_production_male = $read_recordAgric_service_worst_Water_for_production_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Water for production')->where('gender','Male')->count();

                $read_recordAgric_service_worst_Water_for_production_female = $read_recordAgric_service_worst_Water_for_production_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Water for production')->where('gender','Female')->count();

                // ======================================================================

                


                // Access to Credit and capital QN.1 best
                $read_recordAgric_service_best_Access_to_Credit_and_capital_male = $read_recordAgric_service_best_Access_to_Credit_and_capital_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Access to Credit and capital')->where('gender','Male')->count();

                $read_recordAgric_service_best_Access_to_Credit_and_capital_female = $read_recordAgric_service_best_Access_to_Credit_and_capital_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Access to Credit and capital')->where('gender','Female')->count();

                // Worst

                $read_recordAgric_service_worst_Access_to_Credit_and_capital_male = $read_recordAgric_service_worst_Access_to_Credit_and_capital_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Access to Credit and capital')->where('gender','Male')->count();

                $read_recordAgric_service_worst_Access_to_Credit_and_capital_female = $read_recordAgric_service_worst_Access_to_Credit_and_capital_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Access to Credit and capital')->where('gender','Female')->count();

                // ==========================================================================================

                

                // Agricultural mechanization QN.1 best
                $read_recordAgric_service_best_Agricultural_mechanization_male = $read_recordAgric_service_best_Agricultural_mechanization_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Agricultural mechanization')->where('gender','Male')->count();

                $read_recordAgric_service_best_Agricultural_mechanization_female = $read_recordAgric_service_best_Agricultural_mechanization_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Agricultural mechanization')->where('gender','Female')->count();

                // worst

                $read_recordAgric_service_worst_Agricultural_mechanization_male = $read_recordAgric_service_worst_Agricultural_mechanization_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Agricultural mechanization')->where('gender','Male')->count();

                $read_recordAgric_service_worst_Agricultural_mechanization_female = $read_recordAgric_service_worst_Agricultural_mechanization_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Agricultural mechanization')->where('gender','Female')->count();

                // ===========================================================

                
                // Market linkages QN.1 best
                $read_recordAgric_service_best_Market_linkages_male = $read_recordAgric_service_best_Market_linkages_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Market linkages')->where('gender','Male')->count();

                $read_recordAgric_service_best_Market_linkages_female = $read_recordAgric_service_best_Market_linkages_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Market linkages')->where('gender','Female')->count();

                // worst

                $read_recordAgric_service_worst_Market_linkages_male = $read_recordAgric_service_worst_Market_linkages_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Market linkages')->where('gender','Male')->count();

                $read_recordAgric_service_worst_Market_linkages_female = $read_recordAgric_service_worst_Market_linkages_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Market linkages')->where('gender','Female')->count();

                // ======================================================================

                

                 // Capacity building opportunities in various agriculture value chains QN.1 Best
                $read_recordAgric_service_best_Capacity_building_male = $read_recordAgric_service_best_Capacity_building_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Capacity building opportunities in various agriculture value chains')->where('gender','Male')->count();

                $read_recordAgric_service_best_Capacity_building_female = $read_recordAgric_service_best_Capacity_building_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','Capacity building opportunities in various agriculture value chains')->where('gender','Female')->count();

                // Worst

                $read_recordAgric_service_worst_Capacity_building_male = $read_recordAgric_service_worst_Capacity_building_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Capacity building opportunities in various agriculture value chains')->where('gender','Male')->count();

                $read_recordAgric_service_worst_Capacity_building_female = $read_recordAgric_service_worst_Capacity_building_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Capacity building opportunities in various agriculture value chains')->where('gender','Female')->count();


                // ============================================================================

                


                 // value addition tools at the sub county QN.1
                $read_recordAgric_service_best_value_addition_male = $read_recordAgric_service_best_value_addition_male + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','value addition tools at the sub county')->where('gender','Male')->count();

                $read_recordAgric_service_best_value_addition_female = $read_recordAgric_service_best_value_addition_female + Record::all()->where('sub_county_id',$subcounty->id)->where('Agric_service_best','value addition tools at the sub county')->where('gender','Female')->count();

                // Worst
                $read_recordAgric_service_worst_value_addition_male = $read_recordAgric_service_worst_value_addition_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','value addition tools at the sub county')->where('gender','Male')->count();

                $read_recordAgric_service_worst_value_addition_female = $read_recordAgric_service_worst_value_addition_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','value addition tools at the sub county')->where('gender','Female')->count();

                // ===========================================================================

                

                // worst

                $read_recordAgric_service_worst_address_challenges_male = $read_recordAgric_service_worst_address_challenges_male + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Support to address challenges of climate change')->where('gender','Male')->count();

                $read_recordAgric_service_worst_address_challenges_female = $read_recordAgric_service_worst_address_challenges_female + Record::all()->where('sub_county_id',$subcounty->id)->where('agric_service_worst','Support to address challenges of climate change')->where('gender','Female')->count();

                // ========================================================================
          
     

      $total_best_service = array();

        // Agricultural Extension services QN.1
       $gender_arrayAgric_service_best_age_Agric_service_best["Male"] = $read_recordAgric_service_best_male;
       $gender_arrayAgric_service_best_age_Agric_service_best["Female"] = $read_recordAgric_service_best_female;


        $problem["Support to address challenges of climate change"] = $Agric_service_problem_Market_address_challange;
        $problem["value addition tools at the sub county"] = $Agric_service_problem_Market_value_addition;
        $problem["Capacity building opportunities in various agriculture value chains"] = $Agric_service_problem_capacity_building;
        $problem["Market linkages"] = $Agric_service_problem_Market_linkages;
        $problem["Agricultural mechanization"] = $Agric_service_problem_Agricultural_mechanization;
        $problem["Access to Credit and capital"] = $Agric_service_problem_Access_to_Credit_and_capital;
        $problem["Access to Credit and capital"] = $Agric_service_problem_Water_for_production;
        $problem["Advisory services"] = $Agric_service_problem_Advisory_services;
        $problem["Fertilizers"] = $Agric_service_problem_Fertilizers;
        $problem["Pesticides"] = $Agric_service_problem_Pesticides;
        $problem["Seeds"] = $Agric_service_problem_Seeds;
        $problem["Agricultural Extension services"] = $Agric_service_problem_Agricultural_extension_services;

        $priority["Pesticides"] = $Agric_Pesticides;
        $priority["Seeds"]=$Agric_proposed_priority_Seeds;
        $priority["Fertilizers"] = $Agric_Fertilizers;
        $priority["Advisory services"] = $Agric_Advisory_services;
        $priority["Water for production"] = $Agric_Water_for_production;
        $priority["Access to Credit and capital"] = $Agric_Access_Credit_capital;
        $priority["Agricultural mechanization"] = $Agric_Agricultural_mechanization;
        $priority["Market linkages"] = $Agric_Market_linkages;
        $priority["Capacity building opportunities in various agriculture value chains"] = $Agric_Capacity_building;
        $priority["Value addition tools at the sub county"] = $Agric_value_addition;
        $priority["Support to address challenges of climate change"] = $Agric_Support_address; 
    

       $total_Extension_services = $read_recordAgric_service_best_male + $read_recordAgric_service_best_female;
       // $total_best_service->name = "Agricultural Extension services";
       // $total_best_service->value = $total_Extension_services;
       $total_best_service["Agricultural Extension services"] = (int)$total_Extension_services;

       $worst_service["Agricultural Extension services"] = (int)($read_recordAgric_service_best_male_worst+$read_recordAgric_service_best_female_worst);
        // Seed QN.1
       $gender_arrayAgric_service_best_age_Agric_service_best_seed["Male"] = $read_recordAgric_service_best_Seeds_ageCategory_b18;
       $gender_arrayAgric_service_best_age_Agric_service_best_seed["Female"] = $read_recordAgric_service_best_Seeds_ageCategory_b29;
      

       $total_seed = $read_recordAgric_service_best_Seeds_ageCategory_b18 + $read_recordAgric_service_best_Seeds_ageCategory_b29;
       $total_best_service["Seeds"] = (int)$total_seed;

       $worst_service["Seeds"] = (int)($read_recordAgric_service_worst_Seeds_male+$read_recordAgric_service_worst_Seeds_female);

       // Pesticides QN.1
       $gender_arrayAgric_service_best_age_Agric_service_best_Pesticides["Male"] = $read_recordAgric_service_best_Pesticides_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_Pesticides["Female"] = $read_recordAgric_service_best_Pesticides_female;
     

       $total_Pesticides = $read_recordAgric_service_best_Pesticides_male + $read_recordAgric_service_best_Pesticides_female;
       $total_best_service["Pesticides"] = (int)$total_Pesticides;

       $worst_service["Pesticides"] = (int)($read_recordAgric_service_worst_Pesticides_male + $read_recordAgric_service_worst_Pesticides_female);

       // Fertilizers QN
       $gender_arrayAgric_service_best_age_Agric_service_best_Fertilizers["Male"] = $read_recordAgric_service_best_Fertilizers_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_Fertilizers["Female"] = $read_recordAgric_service_best_Fertilizers_female;
    

       $total_Fertilizers = $read_recordAgric_service_best_Fertilizers_male + $read_recordAgric_service_best_Fertilizers_female;
       $total_best_service["Fertilizers"] = (int)$total_Fertilizers;

       $worst_service["Fertilizers"] = (int)($read_recordAgric_service_worst_Fertilizers_male+$read_recordAgric_service_worst_Fertilizers_female);

        // Advisory_services QN 1 
       $gender_arrayAgric_service_best_age_Agric_service_best_Advisory_services["Male"] = $read_recordAgric_service_best_Advisory_services_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_Advisory_services["Female"] = $read_recordAgric_service_best_Advisory_services_female;
        

       $total_Advisory_services = $read_recordAgric_service_best_Advisory_services_male + $read_recordAgric_service_best_Advisory_services_female;

       $total_best_service["Advisory services"]=(int)$total_Advisory_services;

       $worst_service["Advisory services"] = (int)($read_recordAgric_service_worst_Advisory_services_male+$read_recordAgric_service_worst_Advisory_services_female);

       // Water_for_production QN.1
       $gender_arrayAgric_service_best_age_Agric_service_best_Water_for_production["Male"] = $read_recordAgric_service_best_Water_for_production_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_Water_for_production["Female"] = $read_recordAgric_service_best_Water_for_production_female;
     

       $total_Water_for_production = $read_recordAgric_service_best_Water_for_production_male + $read_recordAgric_service_best_Water_for_production_female;
       $total_best_service["Water for production"] = (int)$total_Water_for_production;

       $worst_service["Water for production"] = (int)($read_recordAgric_service_worst_Water_for_production_male + $read_recordAgric_service_worst_Water_for_production_female);

       // Access to Credit and capital QN.1
       $gender_arrayAgric_service_best_age_Agric_service_best_Access_to_Credit_and_capital["Male"] = $read_recordAgric_service_best_Access_to_Credit_and_capital_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_Access_to_Credit_and_capital["Female"] = $read_recordAgric_service_best_Access_to_Credit_and_capital_female;
       

       $total_Access_to_Credit_and_capital = $read_recordAgric_service_best_Access_to_Credit_and_capital_male + $read_recordAgric_service_best_Access_to_Credit_and_capital_female;

       $total_best_service["Access to Credit and capital"]=(int)$total_Access_to_Credit_and_capital;

       $worst_service["Access to Credit and capital"] = (int)($read_recordAgric_service_worst_Access_to_Credit_and_capital_male + $read_recordAgric_service_worst_Access_to_Credit_and_capital_female);

        // Agricultural_mechanization QN 1
       $gender_arrayAgric_service_best_age_Agric_service_best_Agricultural_mechanization["Male"] = $read_recordAgric_service_best_Agricultural_mechanization_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_Agricultural_mechanization["Female"] =$read_recordAgric_service_best_Agricultural_mechanization_female;
       

       $total_Agricultural_mechanization = $read_recordAgric_service_best_Market_linkages_male + $read_recordAgric_service_best_Market_linkages_female;

       $total_best_service["Agricultural mechanization"]=(int)$total_Agricultural_mechanization;

       $worst_service["Agricultural mechanization"]=(int)($read_recordAgric_service_worst_Agricultural_mechanization_male + $read_recordAgric_service_worst_Agricultural_mechanization_female);

       // Market linkages QN.1
       $gender_arrayAgric_service_best_age_Agric_service_best_Market_linkages["Male"] = $read_recordAgric_service_best_Market_linkages_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_Market_linkages["Female"] = $read_recordAgric_service_best_Market_linkages_female;
      

       $total_Market_linkages = $read_recordAgric_service_best_Market_linkages_male + $read_recordAgric_service_best_Market_linkages_female;

       $total_best_service["Market linkages"] = (int)$total_Market_linkages;

       $worst_service["Market linkages"] = (int)($read_recordAgric_service_worst_Market_linkages_male+$read_recordAgric_service_worst_Market_linkages_female);

       // Capacity_building QN.1
       $gender_arrayAgric_service_best_age_Agric_service_best_Capacity_building["Male"] = $read_recordAgric_service_best_Capacity_building_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_Capacity_building["Female"] = $read_recordAgric_service_best_Capacity_building_female;
       

       $total_Capacity_building = $read_recordAgric_service_best_Capacity_building_male + $read_recordAgric_service_best_Capacity_building_female;

       $total_best_service["Capacity building opportunities in various agriculture value chains"]=(int)$total_Capacity_building;

       $worst_service["Capacity building opportunities in various agriculture value chains"]=(int)($read_recordAgric_service_worst_Capacity_building_male + $read_recordAgric_service_worst_Capacity_building_female);

        // value_addition QN.1
       $gender_arrayAgric_service_best_age_Agric_service_best_value_addition["Male"] = $read_recordAgric_service_best_value_addition_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_value_addition["Female"] = $read_recordAgric_service_best_value_addition_female;
        

       $total_value_addition = $read_recordAgric_service_best_value_addition_male + $read_recordAgric_service_best_value_addition_female;

       $total_best_service["value addition tools at the sub county"]=(int)$total_value_addition;

       $worst_service["value addition tools at the sub county"] = (int)($read_recordAgric_service_worst_value_addition_male + $read_recordAgric_service_worst_value_addition_female);

       // address_challenges No. 1
       $gender_arrayAgric_service_best_age_Agric_service_best_address_challenges["Male"] = $read_recordAgric_service_best_address_challenges_male;
       $gender_arrayAgric_service_best_age_Agric_service_best_address_challenges["Female"] = $read_recordAgric_service_best_address_challenges_female;
  

       $total_address_challenges = $read_recordAgric_service_best_address_challenges_male + $read_recordAgric_service_best_address_challenges_female;

       $total_best_service["Support to address challenges of climate change"] = (int)$total_address_challenges;

       $worst_service["Support to address challenges of climate change"] = (int)($read_recordAgric_service_worst_address_challenges_male + $read_recordAgric_service_worst_address_challenges_female);

        $gender['Male'] = $read_male;
        $gender['Female'] = $read_female;

        $age["Below 18 years"]=$read_ageCategory_b18;
        $age["18 to 29 years"]=$read_ageCategory_b29;
        $age["30 to 35 years"]=$read_ageCategory_b30;
        $age["Above 35 years"]=$read_ageCategory_a30;

        $title_qn_one = "In ".$subcounty->name." Sub-County, which agricultural service has been the best?";
        $title_qn_two = "In ".$subcounty->name." Sub-County, which agricultural service has been the worst?";
        $title_qn_three = "In ".$subcounty->name." Sub-County, What do you see as the biggest problem facing Agriculture?";

         $title_qn_five = "In ".$subcounty->name." Sub-County, What is your proposed priority?";

        $title =  $subcounty->district->region->name." Region , ". $subcounty->district->name." District, ". $subcounty->name." Sub-County";
   
       return view("pages.dashboard")->with(compact('gender_arrayAgric_service_best_age_Agric_service_best_address_challenges','agric_service_best_age_seed_best_value_addition','gender_arrayAgric_service_best_age_Agric_service_best_value_addition','agric_service_best_age_seed_best_Capacity_building','gender_arrayAgric_service_best_age_Agric_service_best_Capacity_building','agric_service_best_age_seed_best_Market_linkages','gender_arrayAgric_service_best_age_Agric_service_best_Market_linkages','agric_service_best_age_seed_best_Agricultural_mechanization','gender_arrayAgric_service_best_age_Agric_service_best_Agricultural_mechanization','agric_service_best_age_seed_best_Access_to_Credit_and_capital','gender_arrayAgric_service_best_age_Agric_service_best_Access_to_Credit_and_capital','agric_service_best_age_seed_best_Water_for_production','gender_arrayAgric_service_best_age_Agric_service_best_Water_for_production','agric_service_best_age_seed_best_Advisory_services','gender_arrayAgric_service_best_age_Agric_service_best_Advisory_services','agric_service_best_age_seed_best_Fertilizers','gender_arrayAgric_service_best_age_Agric_service_best_Fertilizers','agric_service_best_age_seed_best_Pesticides','gender_arrayAgric_service_best_age_Agric_service_best_Pesticides','agric_service_best_age_seed_best_seed','gender_arrayAgric_service_best_age_Agric_service_best_seed','gender_arrayAgric_service_best_age_seed_best','gender_arrayAgric_service_best_age_Agric_service_best','title_qn_one','total_best_service','worst_service','title_qn_two','gender','age','problem','title_qn_three','title','title_qn_three','title','priority','title_qn_five'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
