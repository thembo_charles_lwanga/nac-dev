<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCounty extends Model
{
    public function parish($value='')
    {
    	return $this->hasMany('App\Parish');
    }
    public function district($value='')
    {
    	return $this->belongsTo('App\District','district_id');
    }
}
