<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('parish_id')->unsigned();
            $table->string('record_id')->unique();
            $table->string('staffName');
            $table->string('date_recorded')->nullable();
            $table->string('ageCategory')->nullable();
            $table->string('gender')->nullable();

            $table->string('Agric_service_best')->nullable();
            $table->string('agric_service_spec')->nullable();
            $table->string('agric_service_reason')->nullable();

            $table->string('agric_service_worst')->nullable();
            $table->string('agric_service_worst_spec')->nullable();
            $table->string('agric_service_reason_worst')->nullable();

            $table->string('Agric_service_problem')->nullable();
            $table->string('Agric_service_problem_spec')->nullable();
            $table->string('Agric_service_reason_problem')->nullable();

            $table->string('Agric_service_need')->nullable();
            $table->string('Agric_service_need_spec')->nullable();
            $table->string('Agric_service_reason_need')->nullable();

            $table->string('Agric_proposed_priority')->nullable();
            $table->string('Agric_proposed_priority_spec')->nullable();
            $table->string('Agric_proposed_priority_reason')->nullable();

            $table->foreign('parish_id')->references('id')->on('parishes')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
