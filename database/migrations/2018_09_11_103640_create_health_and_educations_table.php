<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHealthAndEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_and_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('record_id')->unique();
            $table->string('ageCategory')->nullable();
            $table->string('gender')->nullable();
            $table->string('level_of_education')->nullable();
            $table->string('establish_health_education')->nullable();
            $table->string('yes_establish_health_education')->nullable();
            $table->string('reason_establish_health_education')->nullable();
            $table->string('rate_establish_health_education')->nullable();
            $table->string('rate_reason_establish_health_education')->nullable();
            $table->string('best_health_education')->nullable();
            $table->string('best_health_education_spec')->nullable();
            $table->string('best_health_education_reason')->nullable();
            $table->string('worst_health_education')->nullable();
            $table->string('worst_health_education_spec')->nullable();
            $table->string('worst_health_education_reason')->nullable();
            $table->string('priority_health_education')->nullable();
            $table->string('priority_health_education_spec')->nullable();
            $table->string('priority_health_education_reason')->nullable();
                       
            $table->integer('sub_county_id')->unsigned();
            $table->foreign('sub_county_id')->references('id')->on('sub_counties')->onUpdate('cascade')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_and_educations');
    }
}
