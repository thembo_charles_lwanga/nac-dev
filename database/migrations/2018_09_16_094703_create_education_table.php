<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education', function (Blueprint $table) {
                       $table->increments('id');
            $table->timestamps();
            $table->string('record_id')->unique();
            $table->string('ageCategory')->nullable();
            $table->string('gender')->nullable();
            $table->string('level_of_education')->nullable();       
            $table->string('use_education')->nullable();
            $table->string('best_use_education_service')->nullable();
            $table->string('best_use_education_spec')->nullable();
            $table->string('best_use_education_reason')->nullable();
            $table->string('worst_use_education_service')->nullable();
            $table->string('worst_use_education_spec')->nullable();
            $table->string('worst_use_education_reason')->nullable();
            $table->string('priority_use_education_service')->nullable();
            $table->string('priority_use_education_spec')->nullable();
            $table->string('priority_use_education_reason')->nullable();
           
            $table->integer('sub_county_id')->unsigned();
            $table->foreign('sub_county_id')->references('id')->on('sub_counties')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education');
    }
}
