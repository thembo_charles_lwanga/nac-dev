@extends('layouts.master')

@section('content')
<div class="card-box">
  <div class="table-responsive"> 
          <table class="table table-hover" id="Agricultural_Extension_services">
              <thead>
                  <th>#</th> <th>Record id</th> <th>Region</th> <th>District</th> <th>Sub - county</th> <th>Parish</th> <th>Staff Name</th> <th>Date recorded</th> <th>Age Category</th> <th>Gender</th> <th>Agric service best</th> <th>Agric service spec</th> <th>Agric service reason</th> <th>Agric service worst</th> <th>Agric service worst spec</th> <th>Agric service reason worst</th> <th>Agric service problem</th> <th>Agric service problem spec</th> <th>Agric service reason problem</th> <th>Priority</th> <th>Priority Spec</th> <th>Priority reason</th>
              </thead>

              <tbody>
                  @foreach($record as $records)
                    <tr>
                        <td>{{$records->id}}</td>
                        <td>{{$records->record_id}}</td> 
                        <td>{{$records->subcounty->district->region->name}}</td>  
                        <td>{{$records->subcounty->district->name}}</td> 
                        <td>{{$records->subcounty->name}}</td> 
                        <td>{{$records->name}}</td> 
                        <td>{{$records->staffName}}</td> 
                        <td>{{date("d-m-Y",$records->date_recorded)}}</td>
                        <td>{{$records->ageCategory}}</td>
                        <td>{{$records->gender}}</td> 
                        <td>{{$records->Agric_service_best}}</td>
                        <td>{{$records->agric_service_spec}}</td> 
                        <td>{{$records->agric_service_reason}}</td> 
                        <td>{{$records->agric_service_worst}}</td> 
                        <td>{{$records->agric_service_worst_spec}}</td> 
                        <td>{{$records->agric_service_reason_worst}}</td> 
                        <td>{{$records->Agric_service_problem}}</td> 
                        <td>{{$records->Agric_service_problem_spec}}</td> 
                        <td>{{$records->Agric_service_reason_problem}}</td>
                        <td>{{$records->Agric_proposed_priority}}</td>
                        <td>{{$records->Agric_proposed_priority_spec}}</td>
                        <td>{{$records->Agric_proposed_priority_reason}}</td>
                    </tr>
                  @endforeach
              </tbody>                        
          </table>
      </div>
    </div>
            
@endsection

 
