@extends('layouts.master')
@section('content')
 
  <h1>{{ $title}}</h1>

<h2>Number of respondents</h2>
 <div class="row"> 
  <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">                      
    <div class="card-box">                   
      <div id="total_best_service_gender" style="height: 400px; margin: 0 auto"></div>
    </div>  
  </div>

   <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
      <div class="card-box">                   
        <div id="total_best_service_age" style="height: 400px; margin: 0 auto"></div>
      </div>    
  </div>
</div>



<h2>{{$title_qn_one}}</h2>

<!-- gental best -->
 <div class="row"> 
  <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
        <div class="card-box">                   
            <div id="total_best_service" style="height: 400px; margin: 0 auto"></div>
          
      </div>
  </div>

   <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="card-box">                   
            <div id="total_best_service_pie" style="height: 400px; margin: 0 auto"></div>
           
      </div>
  </div>
</div>
<!-- broken best -->


<br><br>
 <div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
           <div class="card-box"> 
           <table class="table" id="Agricultural_Extension_services">
             <thead>
               <th>Service</th> <th>Number of poeple</th>
             </thead>
              <tbody>
               @foreach($total_best_service as $key => $total_best_services)
               <tr>
                 <td>{{ $key}}</td> <td>{{$total_best_services}}</td>
               </tr>
               @endforeach
             </tbody>
           </table>
      
      </div>
  </div>
</div>

<h2>{{$title_qn_two}}</h2>
<h5>The one with a bigger value is the Worst</h5>
 <div class="row"> 
  <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
           <div class="card-box">                   
            <div id="total_worst_service" style="height: 400px; margin: 0 auto"></div>
          </div>
      
  </div>

   <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <div class="card-box">                    
            <div id="total_worst_service_pie" style="height: 400px; margin: 0 auto"></div>
          </div>
     
  </div>
</div>

<br><br>
 <div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="card-box"> 
           <table class="table" id="Agricultural_Extension_services_worst">
             <thead>
               <th>Service</th> <th>Number of poeple</th>
             </thead>
              <tbody>
               @foreach($worst_service as $key => $total_best_services)
               <tr>
                 <td>{{ $key}}</td> <td>{{$total_best_services}}</td>
               </tr>
               @endforeach
             </tbody>
           </table>
        </div>
   
  </div>
</div>

<h2>{{$title_qn_three}}</h2>

<div class="row"> 
  <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <div class="card-box">                   
            <div id="total_service_problem" style="height: 400px; margin: 0 auto"></div>
          </div>
     
  </div>

   <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <div class="card-box">                  
            <div id="total_service_problem_bar" style="height: 400px; margin: 0 auto"></div>
          </div>
     
  </div>
</div>

<br><br>
 <div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="card-box"> 
           <table class="table" id="Agricultural_Extension_services_worst">
             <thead>
               <th>Service</th> <th>Number of poeple</th>
             </thead>
              <tbody>
               @foreach($problem as $key => $total_best_services)
               <tr>
                 <td>{{$key}}</td> <td>{{$total_best_services}}</td>
               </tr>
               @endforeach
             </tbody>
           </table>
        </div>
      </div>
 
</div>


<h2>{{$title_qn_five}}</h2>

<div class="row"> 
  <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <div class="card-box">                    
            <div id="total_priority" style="height: 400px; margin: 0 auto"></div>
          </div>
   
  </div>

   <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <div class="card-box">                    
            <div id="total_priority_bar" style="height: 400px; margin: 0 auto"></div>
          </div>
      
  </div>
</div>

<br><br>
 <div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="card-box"> 
           <table class="table" id="Agricultural_Extension_services_worst">
             <thead>
               <th>Service</th> <th>Number of poeple</th>
             </thead>
              <tbody>
               @foreach($priority as $key => $total_best_services)
               <tr>
                 <td>{{$key}}</td> <td>{{$total_best_services}}</td>
               </tr>
               @endforeach
             </tbody>
           </table>
        </div>      
  </div>
</div>


@endsection

@push('scripts')
  <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($problem) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Gender');
        data.addColumn('number', 'No. of poeple');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'Biggest problem facing Agriculture',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.BarChart(document.getElementById('total_service_problem'));
        chart.draw(data, options);        
      }
  </script>

     <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($problem) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Gender');
        data.addColumn('number', 'No. of poeple');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'Biggest problem facing Agriculture',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.PieChart(document.getElementById('total_service_problem_bar'));
        chart.draw(data, options);        
      }
  </script>

  <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($gender) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Gender');
        data.addColumn('number', 'No. of poeple');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'Sample space by Gender',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.PieChart(document.getElementById('total_best_service_gender'));
        chart.draw(data, options);        
      }
  </script>

    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($age) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Age range');
        data.addColumn('number', 'No. of poeple');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'Sample space by age range',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.PieChart(document.getElementById('total_best_service_age'));
        chart.draw(data, options);        
      }
  </script>

 <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($total_best_service) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Agricultural Service');
        data.addColumn('number', 'No. of votes');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'Best Agricultural services',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.BarChart(document.getElementById('total_best_service'));
        chart.draw(data, options);        
      }
    </script>

     <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($total_best_service) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Agricultural Service');
        data.addColumn('number', 'No. of votes');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'Best Agricultural services',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.PieChart(document.getElementById('total_best_service_pie'));
        chart.draw(data, options);        
      }
    </script>

    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($worst_service) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Agricultural Service');
        data.addColumn('number', 'No. of votes');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'Worst Agricultural services',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.BarChart(document.getElementById('total_worst_service'));
        chart.draw(data, options);        
      }
    </script>

    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($worst_service) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Stages');
        data.addColumn('number', 'No. of votes');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'Worst Agricultural services',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.PieChart(document.getElementById('total_worst_service_pie'));
        chart.draw(data, options);        
      }
    </script>

    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($priority) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Priority');
        data.addColumn('number', 'No. of votes');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'proposed priorities',
          is3D: true,
          curveType: 'none',
        };        
        var chart1 = new google.visualization.PieChart(document.getElementById('total_priority'));
        chart1.draw(data, options);

        var chart2 = new google.visualization.BarChart(document.getElementById('total_priority_bar'));
        chart2.draw(data, options);
      }
    </script> 
@endpush