@extends('layouts.master')
@section('content')
<h2>Number of respondents</h2>
 <div class="row"> 
  <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">                      
    <div class="card-box">                   
      <div id="total_best_service_gender" style="height: 400px; margin: 0 auto"></div>
    </div>  
  </div>

   <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
      <div class="card-box">                   
        <div id="total_best_service_age" style="height: 400px; margin: 0 auto"></div>
      </div>    
  </div>
</div>

<h1>Distributive data analysis</h1>
<div class="card-box">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
     @foreach($regions as $region)
      <p class="alert alert-warning"> <a href="{{route('region.show',$region->id)}}">{{$region->name}} Region</a></p>
       @foreach($region->district as $districts)                        
            <p style="margin-left: 30px;"><a href="{{route('district.show',$districts->id)}}"> {{$districts->name}} District </a></p>
            @foreach($districts->subcounty as $sub_country)
              <p style="margin-left: 80px;"><a href="{{route('sub_county.show',$sub_country->id)}}">{{$sub_country->name}} Sub-County</a></p>
             
            @endforeach
        @endforeach
     @endforeach                     
</div>         
@endsection

@push('scripts')
  <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($gender) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Gender');
        data.addColumn('number', 'No. of poeple');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'All respondents by gender',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.PieChart(document.getElementById('total_best_service_gender'));
        chart.draw(data, options);        
      }
  </script>

     <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {    
       var record={!! json_encode($category) !!};
       console.log(record);
       // Create our data table.
       var data = new google.visualization.DataTable();
        data.addColumn('string', 'Categories');
        data.addColumn('number', 'No. of poeple');
       for(var k in record){
            var v = record[k];
           
             data.addRow([k,v]);
          console.log(v);
          }
        var options = {
          title: 'All respondents by category',
          is3D: true,
          curveType: 'none',
        };        
        var chart = new google.visualization.PieChart(document.getElementById('total_best_service_age'));
        chart.draw(data, options);        
      }
  </script>

  @endpush