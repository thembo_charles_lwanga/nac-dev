@extends('layouts.master')

@section('content')
<div class="card-box">
  <div class="table-responsive"> 
          <table class="table table-hover" id="Agricultural_Extension_services">
              <thead>
              	<th>#</th>
              	<th>Record id</th>
              	<th>Region</th>
              	<th>District</th>
              	<th>Sub-county</th>
              	<th>Age Category</th>
              	<th>Gender</th>
              	<th>Level of education</th>              	 
              	<th>UPE & USE education</th>
              	<th>Best UPE & USE </th>
              	<th>Best UPE & USE (reason)</th>
              	<th>Worst UPE & USE</th>
              	<th>Worst UPE & USE (Reason)</th>
              	<th>Priority UPE & USE</th>
              	<th>Priority UPE & USE(Reason)</th>

               </thead>

              <tbody>
              	@foreach($health_and_education as $header)
              	<tr>
              		<td>{{$header->id}}</td>		
              		<td>{{$header->record_id}}</td>		
              		<td>{{$header->subcounty->district->region->name}}</td>  
                    <td>{{$header->subcounty->district->name}}</td> 
                    <td>{{$header->subcounty->name}}</td>		
              		<td>{{$header->ageCategory}}</td>		
              		<td>{{$header->gender}}</td>		
              		<td>{{$header->level_of_education}}</td>		
              	 		
              		<td>{{$header->use_education}}</td>		
              		<td>{{$header->best_use_education_service}} {{$header->best_use_education_spec}}</td>		
              		<td>{{$header->best_use_education_reason}}</td>		
              		<td>{{$header->worst_use_education_service}} {{$header->worst_use_education_spec}}</td>		
              		<td>{{$header->worst_use_education_reason}}</td>		
              		<td>{{$header->priority_use_education_service}} {{$header->priority_use_education_spec}}</td>		
              		<td>{{$header->priority_use_education_reason}}</td>               	
              	</tr>
              	@endforeach

              			 
              </tbody>
          </table>
    </div>
</div>
@endsection