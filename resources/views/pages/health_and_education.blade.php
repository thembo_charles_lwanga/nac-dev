@extends('layouts.master')

@section('content')
<div class="card-box">
  <div class="table-responsive"> 
          <table class="table table-hover" id="Agricultural_Extension_services">
              <thead>
              	<th>#</th>
              	<th>Record id</th>
              	<th>Region</th>
              	<th>District</th>
              	<th>Sub-county</th>
              	<th>Age Category</th>
              	<th>Gender</th>
              	<th>Level of education</th>
              	<th>Health service</th>
              	<th>Health service(Choice)</th>
              	<th>Health service(Reason)</th>
              	<th>Health service(Rate)</th>
              	<th>Health service(Rate reason)</th>
              	<th>Best health Service</th>
              	<th>Best health Service(Reason)</th>
              	<th>Worst health service</th>
              	<th>Worst health service(Reason)</th>
              	<th>Priority health service</th>
              	<th>Priority health service(Reason)</th>
              	 

               </thead>

              <tbody>
              	@foreach($health_and_education as $header)
              	<tr>
              		<td>{{$header->id}}</td>		
              		<td>{{$header->record_id}}</td>		
              		<td>{{$header->subcounty->district->region->name}}</td>  
                    <td>{{$header->subcounty->district->name}}</td> 
                    <td>{{$header->subcounty->name}}</td>		
              		<td>{{$header->ageCategory}}</td>		
              		<td>{{$header->gender}}</td>		
              		<td>{{$header->level_of_education}}</td>		
              		<td>{{$header->establish_health_education}}</td>		
              		<td>{{$header->yes_establish_health_education}}</td>		
              		<td>{{$header->reason_establish_health_education}}</td>		
              		<td>{{$header->rate_establish_health_education}}</td>
              		<td>{{$header->rate_reason_establish_health_education}}</td>		
              		<td>{{$header->best_health_education}} {{$header->best_health_education_spec}}</td>		
              		<td>{{$header->best_health_education_reason}}</td>		
              		<td>{{$header->worst_health_education}}{{$header->worst_health_education_spec}}</td>		
              		<td>{{$header->worst_health_education_reason}}</td>		
              		<td>{{$header->priority_health_education}} {{$header->priority_health_education_spec}}</td>		
              		<td>{{$header->priority_health_education_reason}}</td>		
              		            	
              	</tr>
              	@endforeach

              			 
              </tbody>
          </table>
    </div>
</div>
@endsection