<?php
ini_set('max_execution_time', 500);
Route::any('data_handler','DataHundlerController@data_handler');
Route::any('medical_data_handler','HealthAndEducationController@medical_data_handler');
Route::any('education_data_handler','EducationController@education_data_handler');
Route::resource('data','DataHundlerController');
