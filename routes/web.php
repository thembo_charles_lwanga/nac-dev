<?php
ini_set('max_execution_time', 800000);
ini_set('default_socket_timeout', 600000);

Auth::routes();
Route::get('/', 'DistrictController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('district','DistrictController');
Route::resource('parish','ParishController');
Route::resource('region','RegionController');
Route::resource('sub_county','SubCountyController');
Route::resource('health','HealthAndEducationController');
Route::resource('education','EducationController');
Route::get('test',function(){
	echo phpinfo();
});